function activateButton() {

  let buttons = document.querySelectorAll(".btn");

  document.body.addEventListener("keypress", (e) => {
    for (let btn of buttons) {
      if (btn.textContent.toLowerCase() === e.key.toLowerCase()) {
        btn.classList.add("active");
      } else {
        btn.classList.remove("active");
      }
    }
  });
}

activateButton();